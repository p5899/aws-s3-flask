from typing import BinaryIO
from connection import clientS3
from botocore.exceptions import ClientError
from flask import jsonify, send_file


def upload_file(data: BinaryIO, bucket: str, filename: str):
    try:
        clientS3.upload_fileobj(Fileobj=data, Bucket=bucket, Key=filename)
        return jsonify({
            "message": "success"
        })
    except ClientError as e:
        response = jsonify(e.response["Error"])
        response.status_code = 500
        return response


def get_file(bucket: str, filename: str):
    try:
        response = clientS3.get_object(Bucket=bucket, Key=filename)

        mimetype = "image/" + filename.split(".")[1]
        return send_file(response["Body"], mimetype=mimetype, as_attachment=False)
    except ClientError as e:
        response = jsonify(e.response["Error"])
        response.status_code = 500
        return response


def download_file(bucket: str, filename: str):
    try:
        response = clientS3.get_object(Bucket=bucket, Key=filename)

        mimetype = "image/" + filename.split(".")[1]
        return send_file(response["Body"], mimetype=mimetype, as_attachment=True, download_name=filename)
    except ClientError as e:
        response = jsonify(e.response["Error"])
        response.status_code = 500
        return response


def delete_file(bucket: str, filename: str):
    try:
        response = clientS3.delete_object(Bucket=bucket, Key=filename)
        return response
    except ClientError as e:
        response = jsonify(e.response["Error"])
        response.status_code = 500
        return response
