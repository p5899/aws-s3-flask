from connection import clientS3
from botocore.exceptions import ClientError
from flask import jsonify


def create_bucket(bucket: str):
    try:
        response = clientS3.create_bucket(Bucket=bucket)
        return response
    except ClientError as e:
        response = jsonify(e.response["Error"])
        response.status_code = 500
        return response


def delete_bucket(bucket: str):
    try:
        response = clientS3.delete_bucket(Bucket=bucket)
        return response
    except ClientError as e:
        response = jsonify(e.response["Error"])
        response.status_code = 500
        return response


def list_buckets():
    try:
        response = clientS3.list_buckets()
        return response
    except ClientError as e:
        response = jsonify(e.response["Error"])
        response.status_code = 500
        return response