from flask import Flask
from dotenv import load_dotenv
from routes.bucket import routes_bucket
from routes.file import routes_file


app = Flask(__name__)

app.register_blueprint(routes_file)
app.register_blueprint(routes_bucket)


if __name__ == '__main__':
    load_dotenv()
    app.run(debug=True, host="0.0.0.0", port=4000)
