from boto3 import client
from os import getenv

clientS3 = client("s3",
                  aws_access_key_id=getenv("AWS_ACCESS_KEY_ID"),
                  aws_secret_access_key=getenv("AWS_SECRET_ACCESS_KEY"),
                  region_name=getenv("REGION_NAME")
                  )
