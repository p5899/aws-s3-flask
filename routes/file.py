from flask import Blueprint, request
from functions.file import delete_file, download_file, get_file, upload_file


routes_file = Blueprint("routes_file", __name__, url_prefix="/file")


@routes_file.post("/upload")
def upload():
    file = request.files["file"]
    data = file.stream
    filename = file.filename
    bucket = request.form["bucket"]
    return upload_file(data, bucket, filename)


@routes_file.get("/get/<string:bucket>/<string:filename>")
def get(bucket, filename):
    return get_file(bucket, filename)


@routes_file.get("/download/<string:bucket>/<string:filename>")
def download(bucket, filename):
    return download_file(bucket, filename)


@routes_file.delete("/delete")
def delete():
    body = request.get_json()
    
    return delete_file(bucket=body["bucket"], filename=body["filename"])