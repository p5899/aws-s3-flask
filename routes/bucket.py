from flask import Blueprint, request

from functions.bucket import create_bucket, delete_bucket, list_buckets


routes_bucket = Blueprint("routes_bucket", __name__, url_prefix="/bucket")

# CREATE BUCKET


@routes_bucket.post("/create")
def create():
    body = request.get_json()
    return create_bucket(body["bucket"])

# GET ALL BUCKETS


@routes_bucket.get("/all")
def list():
    return list_buckets()

# DELETE BUCKET


@routes_bucket.delete("/delete")
def delete():
    body = request.get_json()
    return delete_bucket(body["bucket"])
